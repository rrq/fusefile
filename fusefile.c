/***
    fusefile - overlay a file path with a concatenation of parts of
    other files.

    Copyright (C) 2019-  Ralph Ronnquist

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see
    <http://www.gnu.org/licenses/>.

    This source was inspired by the "null.c" example of the libfuse
    sources, which is distributed under GPL2, and copyright (C)
    2001-2007 Miklos Szeredi <miklos@szeredi.hu>.
*/

#define FUSE_USE_VERSION 33

#include <fuse3/fuse.h>
#include <fuse3/fuse_lowlevel.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/fs.h>

struct Region {
    off_t beg;
    off_t end;
};

#define REGIONKEEP(r) ((r,beg == r.end)?: 0 : 1)

struct Source {
    char *filename;
    ssize_t from;
    ssize_t to;
    ssize_t start; // starting position in concatenated file
    int fd;
    int dirty;
};

#define ENDSOURCE( S ) ( S.start + ( S.to - S.from ) )

static struct {
    struct Source *array;
    int count;
    ssize_t size;
} sources;

static struct {
    time_t atime;
    time_t mtime;
    time_t ctime;
} times;

/**
 * Overlay
 */
static struct Overlay {
    struct Source source;
    struct Region *table;
    size_t count;
    size_t limit;
} overlay; // The current overlay.

static void usage();

/**
 * Find the nearest overlay.table region below pos. Returns the index,
 * or -1 if there is none, i.e. pos < overlay.table[0].
 */
static ssize_t overlay_prior_fragment(off_t pos) {
    size_t lo = 0, hi = overlay.count;
    while ( lo < hi ) {
	size_t m = ( lo + hi ) / 2;
	if ( m == lo ) {
	    return overlay.table[m].beg <= pos? m : -1;
	}
	if ( overlay.table[m].beg <= pos ) {
	    lo = m;
	} else {
	    hi = m;
	}
    }
    return -1;
}

/**
 * Save the entry count for overlay.table as 64-bit integer
 * immediately following the overlay content at the index
 * corresponding to the fused file size.
 */
static void overlay_save_count() {
    lseek( overlay.source.fd, overlay.source.to, SEEK_SET );
    size_t size = sizeof( overlay.count );
    char *p = (char *) &overlay.count ;
    while ( size > 0 ) {
	size_t n = write( overlay.source.fd, p, size );
	if ( n < 0 ) {
	    perror( overlay.source.filename );
	    exit( 1 );
	}
	size -= n;
	p += n;
    }
    if ( overlay.source.dirty++ > 1000 ) {
	fsync( overlay.source.fd );
	overlay.source.dirty = 0;
    }
}

/**
 * Update the on-disk cache of overlay.table between the given
 * indexes. The table is laid out immediately following the table
 * count with each region saved as two 64-bit unsigned integers.
 */
static void overlay_save_table(size_t lo,size_t hi) {
    char *p = (char *) &overlay.table[ lo ];
    size_t pos =  overlay.source.to + sizeof( overlay.count ) +
	lo * sizeof( struct Region );
    size_t size = ( hi - lo ) * sizeof( struct Region );
    if ( pos != lseek( overlay.source.fd, pos, SEEK_SET ) ) {
	fprintf( stderr, "%s: seek error\n", overlay.source.filename );
	exit( 1 );
    }
    while ( size > 0 ) {
	size_t n = write( overlay.source.fd, p, size );
	if ( n < 0 ) {
	    perror( overlay.source.filename );
	    exit( 1 );
	}
	size -= n;
	p += n;
    }
    if ( overlay.source.dirty++ > 1000 ) {
	fsync( overlay.source.fd );
	overlay.source.dirty = 0;
    }
}

/**
 * Insert a new region at index p, with previous portion [p,count]
 * moved up to make space.
 */
static void overlay_insert(size_t p,off_t beg,off_t end) {
    size_t bytes;
    // Grow the table if needed
    if ( overlay.count >= overlay.limit ) {
	overlay.limit = overlay.count + 10;
	bytes = overlay.limit * sizeof( struct Region );
	overlay.table = overlay.table?
	    realloc( overlay.table, bytes ) : malloc( bytes );
    }
    bytes = ( overlay.count++ - p ) * sizeof( struct Region );
    if ( bytes ) {
	memmove( (char*) &overlay.table[ p+1 ],
		 (char*) &overlay.table[ p ],
		 bytes );
    }
    overlay.table[ p ].beg = beg;
    overlay.table[ p ].end = end;
    overlay_save_count();
}

/**
 * Delete the region entry at p by moving the portion [p+1,count]
 * down.
 */
static void overlay_delete(size_t p) {
    size_t bytes = ( --overlay.count - p ) * sizeof( struct Region );
    if ( bytes ) {
	memmove( (char*) &overlay.table[ p ],
		 (char*) &overlay.table[ p+1 ],
		 bytes );
    }
}

/**
 * Mark the given region as updated, i.e. written to the overlay. The
 * mark region may attach to prior marked regions or be a new,
 * separate region. If attaching, it causes the prior regions to
 * expand and the table adjusted by deleting any regions that become
 * fully contained in other regions.
 */
static void overlay_mark(off_t beg,off_t end) {
#if DEBUG
    fprintf( stderr, "overlay_mark( %ld, %ld )\n", beg, end );
#endif
    int deleted = 0;
    ssize_t q;
    ssize_t p = overlay_prior_fragment( beg );
    // p is the nearest region below or at beg (or -1)
    if ( p >= 0 && beg <= overlay.table[p].end ) {
	// p overlaps mark region
	if ( end <= overlay.table[p].end ) {
	    // region p covers mark region already
#if DEBUG
	    fprintf( stderr, "overlay covering ( %ld %ld )\n",
		     overlay.table[p].beg, overlay.table[p].end );
#endif
	    return;
	}
	// the new mark region extends region p
	overlay.table[p].end = end;
	q = p+1;
	while ( q < overlay.count &&
		overlay.table[q].beg <= overlay.table[p].end ) {
	    // Extended region merges with subsequent region
	    if ( overlay.table[p].end < overlay.table[q].end ) {
		overlay.table[p].end = overlay.table[q].end;
	    }
	    overlay_delete( q );
	    deleted++;
	}
	if ( deleted ) {
	    overlay_save_count();
	    q = overlay.count;
	}
	overlay_save_table( p, q );
#if DEBUG
	fprintf( stderr, "overlay expand ( %ld %ld ) deleted %d\n",
		 overlay.table[p].beg, overlay.table[p].end, deleted );
#endif
	return;
    }
    // The prior region p does not expand into new mark region
    p++; // subsequent region 
    if ( p >= overlay.count || end < overlay.table[p].beg ) {
	// New mark region is a separate region at p
	overlay_insert( p, beg, end );
#if DEBUG
	fprintf( stderr, "overlay new ( %ld %ld )\n",
		 overlay.table[p].beg, overlay.table[p].end );
#endif
	overlay_save_table( p, overlay.count );
	return;
    }
    // New marks start before and overlap with region p => change p
    // and handle any subsequent regions being covered
    overlay.table[p].beg = beg;
    q = p+1;
    if ( overlay.table[p].end < end ) {
	overlay.table[p].end = end;
	while ( q < overlay.count &&
		overlay.table[q].beg <= overlay.table[p].end ) {
	    if ( overlay.table[p].end < overlay.table[q].end ) {
		overlay.table[p].end = overlay.table[q].end;
	    }
	    overlay_delete( q );
	    deleted++;
	}
	if ( deleted ) {
	    overlay_save_count();
	    q = overlay.count;
	}
    }
    overlay_save_table( p, q );
#if DEBUG
    fprintf( stderr, "overlay before ( %ld %ld ) deleted %d\n",
	     overlay.table[p].beg, overlay.table[p].end, deleted );
#endif
}

/**
 * Capture overlay filenames for processing after source fragments.
 */
static void overlay_setup(char *filenames) {
    overlay.source.filename = filenames;
}

// Duplicate the source record data at the given index.
static void dup_source_item(int index) {
    sources.count++;
    sources.array = realloc(
	sources.array, sources.count * sizeof( struct Source ) );
    if ( sources.array == 0 ) {
	fprintf( stderr, "** OOM when expanding frament table\n" );
	usage();
    }
    // copy elements from [index] to [count-1] one element down
    size_t size = ( sources.count - index - 1 ) * sizeof( struct Source ); 
    memmove( &sources.array[ index+1 ], &sources.array[ index ], size );
}

#if DEBUG
static void print_source(struct Source *p) {
    fprintf( stderr, "%p { %s, %ld, %ld, %ld, %d }\n",
	     p, p->filename, p->from, p->to, p->start, p-> fd );
}
#endif

static char *range;
static unsigned int c;
static int RANGE(int s,int n ) {
    return ( s == n ) && *(range+c) == 0;
}

static int setup_source(struct Source *p,char *frag) {
    struct stat filestat;
    // Open the fragment file rw if possible, else ro
    // First try the fragment in full, thereafter with range appendix
    if ( stat( frag, &filestat ) == 0 ) {
	p->filename = strdup( frag );
	range = 0;
    } else {
	range = strrchr( frag, '/' ); // last '/'
	p->filename = range? strndup( frag, range - frag ) : frag;
    }
    p->fd = open( p->filename, O_RDWR );
    int rdonly = 0;
    if ( p->fd < 0 ) {
	rdonly = 1;
	p->fd = open( p->filename, O_RDONLY );
    }
    if ( p->fd < 0 ) {
	perror( p->filename );
	return 1; // Error return
    }
    if ( ( range != 0 ) && stat( p->filename, &filestat ) ) {
	perror( p->filename );
	return 1; 
    }
    if ( rdonly ) {
	fprintf( stderr, "** %s opened read-only\n", p->filename );
    }
    p->from = 0;
    if ( S_ISBLK( filestat.st_mode ) ) {
	// Block devices report size differently:
	if ( ioctl( p->fd, BLKGETSIZE64, &filestat.st_size ) < 0 ) {
	    perror( p->filename );
	}
#if DEBUG
	fprintf( stderr, "block device size = %ld\n", filestat.st_size );
#endif
    }
    p->to = filestat.st_size;
    // Process any range variation
    if ( range && *(++range) ) {
	long int a,b;
	if ( 0 ) {
	} else if ( RANGE( sscanf( range, "%ld:%ld%n", &a, &b, &c ), 2 )) {
	    p->from = ( a < 0 )? ( p->to + a ) : a;
	    p->to = ( b < 0 )? ( p->to + b ) : b;
	} else if ( RANGE( sscanf( range, "%ld+%ld%n", &a, &b, &c ), 2 )) {
	    p->from = ( a < 0 )? ( p->to + a ) : a;
	    p->to = ( ( b < 0 )? p->to : p->from ) + b;
	} else if ( RANGE( sscanf( range, "%ld+%n", &a, &c ), 1 )) {
	    p->from = ( a < 0 )? ( p->to + a ) : a;
	} else if ( RANGE( sscanf( range, ":%ld%n", &b, &c ), 1 )) {
	    p->to = ( b < 0 )? ( p->to + b ) : b;
	} else if ( RANGE( sscanf( range, "%ld:%n", &a, &c ), 1 )) {
	    p->from = ( a < 0 )? ( p->to + a ) : a;
	} else if ( RANGE( sscanf( range, "%ld%n", &a, &c ), 1 )) {
	    if ( a >= 0 ) {
		p->from = a;
	    } else {
		p->from = p->to + a;
	    }
	} else if ( RANGE( sscanf( range, ":%n", &c), 0 ) ) {
	    // to end from start
	} else {
	    fprintf( stderr, "** BAD RANGE: %s\n", frag );
	    return 1;
	}
    }
    if ( ( filestat.st_mode &  S_IFMT ) == S_IFCHR ) {
	filestat.st_size = p->to; // Pretend size of character device
    }
    if ( p->from < 0 ) {
	p->from = 0;
    }
    if ( p->to > filestat.st_size ) {
	p->to = filestat.st_size;
    }
    if ( p->from >= p->to || p->from >= filestat.st_size ) {
	fprintf( stderr, "** BAD RANGE: %s [%ld:%ld]\n",
		 frag, p->from, p->to );
	return 1;
    }
    p->start = sources.size; // the fusefile position of fragment
    sources.size += p->to - p->from;
    return 0;
}

static int setup_sources(char **argv,int i,int n) {
    sources.array = calloc( n, sizeof( struct Source ) );
    if ( sources.array == 0 ) {
	return 1;
    }
    sources.count = n;
    int j = 0;
    sources.size = 0;
    for ( ; j < n; i++, j++ ) {
	struct Source *p = sources.array + j;
	if ( setup_source( p, argv[i] ) ) {
	    return 1;
	}
#if DEBUG
	print_source( p );
#endif
    }
    return 0;
}

static int fusefile_getattr(const char *path, struct stat *stbuf,
			    struct fuse_file_info *ffi)
{
#if DEBUG
    fprintf( stderr, "fusefile_getattr( %s )\n", path );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
#if DEBUG
    fprintf( stderr, "getattr %ld\n", sources.size );
#endif
    memset( stbuf, 0, sizeof( struct stat ) );
    stbuf->st_mode = S_IFREG | 0644; // Hmmm
    stbuf->st_nlink = 1;
    stbuf->st_size = sources.size;
    stbuf->st_atime = times.atime;
    stbuf->st_mtime = times.mtime;
    stbuf->st_ctime = times.ctime;
    stbuf->st_uid = getuid();
    stbuf->st_gid = getgid();
    return 0;
}

static int fusefile_chmod(const char *path, mode_t m,
			  struct fuse_file_info *ffi)
{
#if DEBUG
    fprintf( stderr, "fusefile_chmod( %s, %d )\n", path, m );
#endif
    return -1;
}

static int fusefile_open(const char *path, struct fuse_file_info *ffi) {
#if DEBUG
    fprintf( stderr, "fusefile_open( %s, %d )\n", path, ffi->flags );
    fprintf( stderr, "fixing( %d )\n", ffi->flags | O_CLOEXEC );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    // set O-CLOEXEC  for this opening?
    times.atime = time( 0 );
    return 0;
}

static int find_source(off_t offset) {
    int lo = 0;
    int hi = sources.count;
    if ( offset >= sources.size ) {
	return -1;
    }
    while ( lo + 1 < hi ) {
	int m = ( lo + hi ) / 2;
	if ( offset < sources.array[ m ].start ) {
	    hi = m;
	} else {
	    lo = m;
	}
    }
    return lo;
}

static int overlay_merge(char *buf,off_t beg,off_t end) {
#if DEBUG
    fprintf( stderr, "merge %ld %ld\n", beg, end );
#endif
    // Find nearest overlay data before or at beg
    ssize_t p = overlay_prior_fragment( beg );
    if ( p < 0 ) {
	p = 0;
    }
    for ( ; p < overlay.count && overlay.table[p].beg < end; p++ ) {
	if ( overlay.table[p].end < beg ) {
	    continue;
	}
	if ( overlay.table[p].beg > beg ) {
	    size_t delta = overlay.table[p].beg - beg;
	    buf += delta;
	    beg += delta;
	}
	size_t size = ( overlay.table[p].end <= end )?
	    ( overlay.table[p].end - beg ) : ( end - beg ); 
	lseek( overlay.source.fd, beg, SEEK_SET );
	while ( size > 0 ) {
	    size_t n = read( overlay.source.fd, buf, size );
	    size -= n;
	    buf += n;
	    beg += n; //
	}
    }
    return 0;
}

// Read <size> bytes from <offset> in file
static int fusefile_read(const char *path, char *buf, size_t size,
			 off_t off, struct fuse_file_info *ffi)
{
    if( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    size_t rr = 0; // total reading
#if DEBUG
    fprintf( stderr, "fusefile_read %ld + %ld\n", off, size );
#endif
    while ( size > 0 ) {
	int i = find_source( off );
	if ( i < 0 ) {
	    return ( off == sources.size )? rr : -ENOENT;
	}
#if DEBUG
	fprintf( stderr, " item: %d ", i );
	print_source(& sources.array[i] );
#endif
	if ( sources.array[i].fd < 0 ) {
	    return -ENOENT;
	}
	times.atime = time( 0 );
	size_t b = off - sources.array[i].start + sources.array[i].from;
	size_t n = sources.array[i].to - b;
	if ( n > size ) {
	    n = size;
	}
	if ( sources.array[i].dirty ) {
	    fsync( sources.array[i].fd );
	    sources.array[i].dirty = 0;
	}
	if ( lseek( sources.array[i].fd, b, SEEK_SET ) < 0 ) {
	    perror( sources.array[i].filename );
	    return -ENOENT;
	}
	ssize_t r = read( sources.array[i].fd, buf + rr, n );
#if DEBUG
	fprintf( stderr, " got: %ld bytes of %ld at %ld\n", r, n, rr );
#endif
	if ( r < 0 ) {
	    perror( sources.array[i].filename );
	    return -ENOENT;
	}
	if ( r == 0 ) {
	    break;
	}
	if ( overlay.source.filename ) {
	    if ( overlay.source.dirty ) {
		fsync( overlay.source.fd );
		overlay.source.dirty = 0;
	    }
	    int x = overlay_merge( buf + rr, off + rr, off + rr + r );
	    if ( x ) {
		return x;
	    }
	}
	rr += r;
	off += r;
	size -= r;
    }
#if DEBUG
    fprintf( stderr, "  total reading %ld bytes\n", rr );
#endif
    return rr;
}

/**
 * Poll for IO readiness.
 */
int fusefile_poll(const char *path, struct fuse_file_info *ffi,
		  struct fuse_pollhandle *ph, unsigned *reventsp )
{
#if DEBUG
    fprintf( stderr, "fusefile_poll( %s ) %p %d\n", path, ph, *reventsp );
#endif
    if( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    if ( ph ) {
	return fuse_notify_poll( ph );
    }
    return 0;
}

static void overlay_load() {
    lseek( overlay.source.fd, overlay.source.to, SEEK_SET );
    size_t x = 0;
    size_t size = sizeof( overlay.count );
    if ( read( overlay.source.fd, &x, size ) != size ) {
	return;
    }
#if DEBUG
    fprintf( stderr, "overlay: %s with %ld regions\n",
	     overlay.source.filename, x );
#endif
    struct Region f = { 0, 0 };
    size = sizeof( struct Region );
    while ( x-- > 0 ) {
	if ( read( overlay.source.fd, &f, size ) != size ) {
	    fprintf( stderr, "%s: bad meta data\n", overlay.source.filename );
	    exit( 1 );
	}
#if DEBUG
	fprintf( stderr, "overlay region: %ld %ld\n", f.beg, f.end );
#endif
	overlay_mark( f.beg, f.end );
    }
}

/**
 * Inject an overlay fragment into the source table, end return the
 * end of the injected fragment.
 */
static off_t overlay_inject_from_region(off_t beg,off_t end) {
    int index = find_source( beg );
    if ( index < 0 ) {
	fprintf( stderr, "** Injecting %s at %ld failed\n",
		 overlay.source.filename, beg );
	usage();
    }
    if ( end > ENDSOURCE( sources.array[ index ] ) ) {
	end = ENDSOURCE( sources.array[ index ] );
    }
    struct Region frags[3] = {
	{ sources.array[ index ].start, beg },
	{ beg, end },
	{ end, ENDSOURCE( sources.array[ index ] ) } };
#if DEBUG
    int i;
    for ( i = 0; i < 3; i++ ) {
	fprintf( stderr, "frags[%d] = (%ld, %ld)\n",
		 i, frags[i].beg, frags[i].end );
    }
#endif
    ssize_t size = frags[0].end - frags[0].beg;
    if ( size ) {
	// Handle any portion before injection point.
	dup_source_item( index );
	off_t point = sources.array[ index ].from + size;
	sources.array[ index ].to = point;
#if DEBUG
	fprintf( stderr, "item %d ", index );
	print_source( &sources.array[ index ] );
#endif
	// Adjust item after injection point
	index++;
	sources.array[ index ].start = beg;
	sources.array[ index ].from = point;
#if DEBUG
	fprintf( stderr, "item %d adjust ", index );
	print_source( &sources.array[ index ] );
#endif
    }
    size = frags[2].end	- frags[2].beg;
    if ( size ) {
	// Handle any remaining portion following injection fragment
	dup_source_item( index );
	sources.array[ index+1 ].start = frags[2].beg;
	sources.array[ index+1 ].from += frags[1].end - frags[1].beg;
#if DEBUG
	fprintf( stderr, "item %d ", index+1 );
	print_source( &sources.array[ index+1 ] );
#endif
    }
    // Set up the injection fragment
    sources.array[ index ].filename = overlay.source.filename;
    sources.array[ index ].from = beg;
    sources.array[ index ].to = end;
    sources.array[ index ].fd = overlay.source.fd;
#if DEBUG
	fprintf( stderr, "item %d ", index );
	print_source( &sources.array[ index ] );
#endif
    return end;
}

/**
 * Inject the current (unopened) overlay into the source list.
 */
static void overlay_inject() {
    overlay.source.fd = open( overlay.source.filename, O_RDONLY );
    if ( overlay.source.fd < 0 ) {
	perror( overlay.source.filename );
	usage();
    }
    
    if ( lseek( overlay.source.fd, overlay.source.to, SEEK_SET ) < 0 ) {
	perror( overlay.source.filename );
	usage();
    }
    size_t count = 0;
    size_t size = sizeof( overlay.count );
    size_t n;
    if ( ( n = read( overlay.source.fd, &count, size ) ) != size ) {
	if ( n != 0 ) {
	    fprintf( stderr, "** error injecting %s\n",
		     overlay.source.filename );
	    usage();
	}
	fprintf( stderr, "** ignoring empty %s\n", overlay.source.filename );
    }
    if ( count == 0 ) {
	close( overlay.source.fd );
	return;
    }
    size = count * sizeof( struct Region );
    overlay.table = calloc( sizeof( struct Region ), count );
    if ( read( overlay.source.fd, overlay.table, size ) != size ) {
	fprintf( stderr, "** error injecting %s\n", overlay.source.filename );
	usage();
    }
    size_t i;
    for ( i = 0; i < count; i++ ) {
	off_t beg = overlay.table[i].beg;
	while ( beg < overlay.table[i].end ) {
#if DEBUG
	    fprintf( stderr, "inject [%ld,%ld] ", beg, overlay.table[i].end );
	    print_source( &overlay.source );
#endif
	    
	    beg = overlay_inject_from_region( beg, overlay.table[i].end );
	}
    }
    free( overlay.table );
    overlay.table = 0;
}

/**
 * Each stacked overlay file is nested into to source list
 */
static void overlay_post_setup() {
    char *end;
    while ( ( end = strchr( overlay.source.filename, ':' ) ) ) {
	*end = 0; //
	overlay_inject();
	overlay.source.filename = end + 1;
    }
    if ( *overlay.source.filename ) {
	overlay.source.fd = open( overlay.source.filename,
				  O_RDWR | O_CREAT, S_IRUSR | S_IWUSR );
	if ( overlay.source.fd < 0 ) {
	    perror( overlay.source.filename );
	    usage();
	}
	overlay_load();
    }
}

/**
 * Write a full block of data over the sources at the offset
 */
static int write_block(off_t off,const char *buf,size_t size) {
#if DEBUG
    fprintf( stderr, "write_block( %ld, ?, %ld )\n", off, size );
#endif
    if ( overlay.source.filename ) {
	overlay_mark( off, off + size ); // Mark region as written
    }
    while ( size > 0 ) {
	int index = find_source( off ); // index of source file
	if ( index < 0 ) {
	    return -EIO; // past EOF
	}
	struct Source *source = overlay.source.filename?
	    &overlay.source :  &sources.array[ index ];
	off_t from = off - source->start + source->from;
	off_t max = source->to - from;
	if ( lseek( source->fd, from, SEEK_SET ) < 0 ) {
	    return -EIO;
	}
	ssize_t todo = ( size < max )? size : max;
	while ( todo > 0 ) {
	    times.mtime = time( 0 );
	    ssize_t n = write( source->fd, buf, todo );
	    if ( n <= 0 ) {
		return -EIO; // Something wrong
	    }
	    buf += n;
	    todo -= n;
	    size -= n;
	    off += n;
	}
	if ( source->dirty++ >= 1000 ) {
	    fsync( source->fd );
	    source->dirty = 0;
	}
    }
    return 0;
}

static int fusefile_write_buf(const char *path, struct fuse_bufvec *buf,
			      off_t off, struct fuse_file_info *ffi) {
#if DEBUG
    fprintf( stderr, "fusefile_write_buf( %s )\n", path );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    size_t size = 0;
    int i;
    for ( i = 0; i < buf->count; i++ ) {
	struct fuse_buf *p = &buf->buf[i];
	if ( p->flags & FUSE_BUF_IS_FD ) {
#if DEBUG
	    fprintf( stderr, "Content held in a file ... HELP!!\n" );
#endif
	    return -EIO;
	}
	if ( write_block( off, (char*) p->mem, p->size ) < 0 ) {
	    return -EIO;
	}
	size += p->size;
    }
#if DEBUG
    fprintf( stderr, "fusefile_write_buf written %ld\n", size );
#endif
    return size;
}

/**
 * Write a fragment at <off>. This overwrites files.
 */
static int fusefile_write(const char *path, const char *buf, size_t size,
			  off_t off, struct fuse_file_info *ffi)
{
#if DEBUG
    fprintf( stderr, "fusefile_write( %s %ld )\n", path, size );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }

    if ( write_block( off, buf, size ) < 0 ) {
	return -EIO;
    }
    return size;
}

#define PUSHBUF 104857600
/**
 * Write data from overlay to source.
 */
static void push_oly(off_t beg, off_t end) {
    static char * buffer = 0;
    // Pretend that there isn't an overlay
    char *filename = overlay.source.filename;
    if ( buffer == 0 ) {
	buffer = malloc( PUSHBUF );
	if ( buffer == 0 ) {
	    fprintf( stderr, "** OOM!!\n" );
	    exit( 1 );
	}
    }
    overlay.source.filename = 0;
    while ( beg < end ) {
	off_t size = end - beg;
	if ( size > PUSHBUF ) {
	    size = PUSHBUF;
	}
	if ( lseek( overlay.source.fd, beg, SEEK_SET ) < 0 ) {
	    fprintf( stderr, "** Cannot seek overlay at %ld\n", beg );
	    break;
	}
	size = read( overlay.source.fd, buffer, size );
	if ( write_block( beg, buffer, size ) < 0 ) {
	    fprintf( stderr, "** Cannot push %ld bytes at %ld\n", size, beg );
	}
	beg += size;
    }
    overlay.source.filename = filename;
}

static void fusefile_destroy(void *data) {
    char *mnt = (char*) data; // As passed to fuse_main
#if DEBUG
    fprintf( stderr, "fusefile_destroy( %s )\n", mnt? mnt : "" );
#endif
    if ( mnt ) {
	unlink( mnt );
    }
}

static void fsync_all_dirty() {
    int i = 0;
    for ( ; i < sources.count; i++ ) {
	if ( sources.array[i].dirty ) {
	    fsync( sources.array[i].fd );
	    sources.array[i].dirty = 0;
	}
    }
    if ( overlay.source.filename && overlay.source.dirty ) {
	fsync( overlay.source.fd );
	overlay.source.dirty = 0;
    }
}

static int fusefile_flush(const char *path, struct fuse_file_info *ffi) {
#if DEBUG
    fprintf( stderr, "fusefile_flush( %s )\n", path );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    fsync_all_dirty();
    return 0;
}

static int fusefile_release(const char *path, struct fuse_file_info *ffi) {
#if DEBUG
    fprintf( stderr, "fusefile_release( %s, %d )\n", path, fi->flags );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    return 0;
}

static int fusefile_fsync(const char *path, int x,
			  struct fuse_file_info *ffi) {
#if DEBUG
    fprintf( stderr, "fusefile_fsync( %s, %d )\n", path, x );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    fsync_all_dirty();
    return 0;
}

/**
 * 
 */
static int fusefile_truncate(const char *path, off_t len,
			     struct fuse_file_info *ffi) {
#if DEBUG
    fprintf( stderr, "fusefile_truncate( %s, %ld )\n", path, len );
#endif
    if ( strcmp( path, "/" ) != 0 ) {
	return -ENOENT;
    }
    return -EIO;
}

void *fusefile_init(struct fuse_conn_info *fci, struct fuse_config *fc) {
#if DEBUG
    fprintf( stderr, "fusefile_init( %d, %d )\n", fci->async_read, fci->want );
#endif
    // Disable asynchronous operations, both reading and direct I/O
    fci->want &= ~ ( FUSE_CAP_ASYNC_READ | FUSE_CAP_ASYNC_DIO );
#if DEBUG
    fprintf( stderr, "fusefile_init( %d, %d )\n", fci->async_read, fci->want );
#endif
    return 0;
}

/**
 * Dump the current fragmentation to stdout.
 */
static int dump_fragments(int push) {
    int oly = 0;
    int src = 0;
    size_t pos = 0;
    while ( src < sources.count ) {
	size_t x = ( oly < overlay.count )?
	    overlay.table[ oly ].beg : sources.size;
	for ( ; src < sources.count && 
		  ENDSOURCE( sources.array[ src ] ) <= x; src++ ) {
	    // Dump sources.array[src] in full
	    if ( !push ) {
		fprintf( stdout, "%s/%ld:%ld\n",
			 sources.array[ src ].filename,
			 pos - sources.array[ src ].start +
			 sources.array[ src ].from,
			 sources.array[ src ].to );
	    }
	    pos = ENDSOURCE( sources.array[ src ] );
	}
	if ( ( src < sources.count ) && ( sources.array[ src ].start < x ) ) {
	    // Dump sources.array[src] up to x;
	    if ( !push ) {
		fprintf( stdout, "%s/%ld:%ld\n",
			 sources.array[ src ].filename,
			 sources.array[ src ].from +
			 pos - sources.array[ src ].start,
			 x - sources.array[ src ].start +
			 sources.array[ src ].from
			 );
	    }
	    pos = ENDSOURCE( sources.array[ src ] );
	}
	if ( oly < overlay.count ) {
	    if ( !push ) {
		fprintf( stdout, "%s/%ld:%ld\n",
			 overlay.source.filename,
			 overlay.table[ oly ].beg,
			 overlay.table[ oly ].end );
	    } else {
		push_oly( overlay.table[ oly ].beg, overlay.table[ oly ].end );
	    }
	    pos = overlay.table[ oly++ ].end;
	}
	for ( ; src < sources.count &&
		  ENDSOURCE( sources.array[ src ] ) <= pos; src++ ) {
	    // Just skip these fragments.
	}
    }
    return( 0 );
}

static struct fuse_operations fusefile_oper = {
    .getattr = fusefile_getattr,
    // NYI .fgetattr = fusefile_fgetattr,
    .chmod = fusefile_chmod,
    .open = fusefile_open,
    .read = fusefile_read,
    .poll = fusefile_poll,
    .write = fusefile_write,
    .write_buf = fusefile_write_buf,
    .destroy = fusefile_destroy,
    // NYI .access = fusefile_access,
    .flush = fusefile_flush,
    .release = fusefile_release,
    .fsync = fusefile_fsync,
    // NYI .ftruncate = fusefile_ftruncate,
    .truncate = fusefile_truncate,
    //.release = fusefile_release,
    .init = fusefile_init,
};

static void usage() {
    char *usage =
"Usage: fusefile [ <fuse options> ] <mount> <file/from-to> ... \n"
"Mounts a virtual, file that is a concatenation of file fragments\n"
	;
    fprintf( stderr, "%s", usage );
    exit( 1 );
}

/**
 * Set up the arguments for the fuse_main call, adding our own.
 * argv[argc] is the mount point argument
 */
static int setup_argv(int argc,char ***argv) {
    // note: (*argv)[ argc ] is the mount point argument
    char *OURS[] = {
	"-odefault_permissions",
	//"-s", // Forced single-threading
	(*argv)[ argc ]
    };
#define OURSN ( sizeof( OURS ) / sizeof( char* ) )
    int N = argc + OURSN;
    // Allocate new arg array plus terminating null pointer
    char **out = malloc( ( N + 1 ) * sizeof( char* ) ); 
    int i;
    for ( i = 0; i < argc; i++ ) {
	out[ i ] = (*argv)[i];
	//fprintf( stderr, " %s", out[ i ] );
    }
    for ( i = 0; i < OURSN; i++ ) {
	out[ argc + i ] = OURS[i];
	//fprintf( stderr, " %s", out[ i ] );
    }
    out[ N ] = 0;
    //fprintf( stderr, "\n" );
    (*argv) = out;
    return N; // Don't include the terminating null pointer
}

/**
 * Mount a concatenation of files,
 * [ <fuse options> ] <mount> <file/from-to> ...
 */
int main(int argc, char *argv[])
{
    char *mnt;
    int i;
    int fuseargc;
    struct stat stbuf;
    int temporary = 0;
    // Scan past options
    for ( i = 1; i < argc; i++ ) {
	if ( *argv[i] != '-' ) {
	    break;
	}
    }
    if ( i > argc - 2 ) { // At least mount point plus one source
	usage();
    }
    fuseargc = i;
    mnt = argv[ i++ ]; // First non-option argument is the mount pount
    #define OVERLAYTAG "-overlay:"
    if ( strncmp( argv[i], OVERLAYTAG, strlen( OVERLAYTAG ) ) == 0 ) {
	// consume "-overlay:filename[,filename]*"
	overlay_setup( argv[i++] + strlen( OVERLAYTAG ) );
	if ( i >= argc ) {
	    usage();
	}
    }
    if ( setup_sources( argv, i, argc-i ) ) {
	return 1;
    }
    if ( overlay.source.filename ) {
	overlay.source.to = sources.size; // Register total size.
	overlay_post_setup();
    }
    if ( stat( mnt, &stbuf ) == -1 ) {
	int fd = open( mnt, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR );
	if ( fd < 0 ) {
	    perror( mnt );
	    return 1;
	}
	time_t now = time( 0 );
	times.atime = now;
	times.mtime = now;
	times.ctime = now;
	temporary = 1;
	close( fd );
    } else if ( ! S_ISREG( stbuf.st_mode ) ) {
	fprintf( stderr, "mountpoint is not a regular file\n" );
	return 1;
    } else {
	times.atime = stbuf.st_atime;
	times.mtime = stbuf.st_mtime;
	times.ctime = stbuf.st_ctime;
    }

    {
	int fd = open( mnt, O_RDWR, S_IRUSR | S_IWUSR );
	if ( fd < 0 ) {
	    perror( mnt );
	    return 1;
	}
	if ( lseek( fd, sources.size, SEEK_SET ) < 0 ) {
	    return -EIO;
	}
    }
    fuseargc = setup_argv( fuseargc, &argv );
    if ( strcmp( "-dump", argv[ 1 ] ) == 0 ) {
	return dump_fragments( 0 );
    }
    if ( strcmp( "-push", argv[ 1 ] ) == 0 ) {
	return dump_fragments( 1 );
    }
    struct fuse_args args = FUSE_ARGS_INIT( fuseargc, argv );
    struct fuse_cmdline_opts opts = { 0 };
    if ( fuse_parse_cmdline( &args, &opts ) ) {
	return 1;
    }
    fuse_opt_free_args( &args );
    return fuse_main( fuseargc, argv, &fusefile_oper,
		      temporary? opts.mountpoint : NULL );
}
