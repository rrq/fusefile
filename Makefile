BINS = fusefile

default: $(BINS)

ifneq (${DEBUG},)
${BINS}: CFLAGS += -DDEBUG=1 -g
endif

${BINS}: CFLAGS += -Wall -D_FILE_OFFSET_BITS=64
fusefile: LDFLAGS = -lfuse3 -pthread

.INTERMEDIATE: fusefile.o
fusefile.o: fusefile.c

fusefile: fusefile.o
	$(CC) $(CFLAGS) $(CPPFLAGS) $? $(LDFLAGS) $(TARGET_ARCH) -o $@

clean:
	rm -f $(BINS)

README.html: README.adoc
	asciidoctor $< > $@

# Building a debian package

SBINDIR = $(DESTDIR)/usr/sbin
MAN8DIR = $(DESTDIR)/usr/share/man/man8

SBINFILES = fusefile fusedisk
MAN8FILES = fusefile.8

INSTALLTARGETS = $(addprefix $(SBINDIR)/,$(SBINFILES))
INSTALLTARGETS += $(addprefix $(MAN8DIR)/,$(MAN8FILES))

$(SBINDIR)/% $(MAN8DIR)/%: %
	install -D -T $< $@

install: $(INSTALLTARGETS)

deb:
	PREFIX= INCLUDE_PREFIX=/usr dpkg-buildpackage --build=full -uc -us
