.mso www.tmac
.TH fusefile 8
.SH NAME
fusefile, fusedisk \- FUSE file mount for combining file fragments

.SH SYNOPSIS
.B fusefile \fR[\fIfuse-opts\fR] \fBmountpoint\fR \fR[\fIoverlay\fR] \fIfilename/from-to\fR ...
.br
.B fusefile \fB-dump\fR \fR[\fIfuse-opts\fR] \fBmountpoint\fR \fR[\fIoverlay\fR] \fIfilename/from-to\fR ...
.br
.B fusefile \fB-push\fR \fR[\fIfuse-opts\fR] \fBmountpoint\fR \fR[\fIoverlay\fR] \fIfilename/from-to\fR ...
.br
.B fusedisk \fR[\fIfuse-opts\fR] \fBmountpoint\fR \fR[\fIoverlay\fR] \fIfilename/from-to\fR ...

.SH DESCRIPTION

\fBfusefile\fR is a FUSE \fIfile mount\fR that presents a series of
fragments of other files as a contiguous concatenation. Technically it
bind mounts a driver on top of the filename mountpoint to provide
access to the given file fragments as if in a single, contiguous file.

\fBfusefile\fR accepts over-writing on the fused file (i.e. the
mountpoint) which gets distributed accordingly to the fragments. But
neither the fused file nor the fragments can change size; any writing
thus merely over-writes content without truncating fragments. All
fragment files are held open while \fBfusefile\fR is active.

By using the optional \fB-overlay:\fIfilename\fR argument between the
mount point and the fragments, an overlay file may be set up. The
overlay file will then be used by \fBfusefile\fR for capturing writes
to the fused file (i.e. the mountpoint). The overlay file will contain
any new written fused file regions followed by meta data to
distinguish between new, written content and old content that comes
from the fragments.

By instead using the \fB-overlay:\fIlist\fR argument where \fIlist\fR
is a colon-separated list of filenames, \fBfusefile\fR will use those
as an ordered stack of overlays and "inject" them as fragments on top
of each other.

The option \fB-dump\fR as first argument together with a fusefile
setup will print the setup to standard output rather than establishing
a fusefile mount. This is of most use with a prior overlay setup where
then the printout includes the portions of updates that have been
captured in the overlay. The printout is the series of fusefile
fragment argments to give in order to intersperse the captured overlay
portions according to the overlay table.

The option \fB-push\fR as first argument together with a fusefile
setup will push the overlay into the sources (except for
write-protected fragments). This is only of use with a prior overlay
setup where then the updates that have been captured in the overlay
get pushed into the fragments.

\fBfusedisk\fR is a helper script to set up a \fBfusefile\fR as a
block device (via \fIfuseblk\fR) by using the device mapper
(\fBdmsetup\fR) to manage an empty block device mapping where content
is handled at the mountpoint via \fBfusefile\fR. (Note that the same
thing may be done with the device manager directly, but then all
fragments need to be in sectors of N*512 bytes whereas with
\fBfusedisk\fR, only the fused file as a whole is "clipped" at nearest
N*512 bytes below actual size)

.SH FRAGMENT ARGUMENTS

The fragment arguments include the filename of a source file, and
optionally start and end byte positions. All in all there five
variations:

.TP
\fIfilename\fR or \fIfilename/\fR
include all of the file. A pathname that includes "/" must be ended
with an extra "/" since that last "/" separates the filename from the
range detail.

.TP
\fIfilename/start:end\fR
include the range from the given start to end. Either "start" or "end"
or both may be omitted, to mean the beginning and end of the file
respectively. If "start" or "end" are less than 0 then it means
relative to the end of the file.

.TP
\fIfilename/start+length\fR
include "length" bytes from the given start. A negative "start" means
relative to the end of the file. If "length" is negative or omitted it
means that position relative to the end.

.TP
\fIfilename/start\fR include bytes from the given start. This is the
same as "/start+"

.P
Note that a negative start position is clipped to 0 and a too large
end position is clipped to the end of the file.

.P
Character devices are treated as being of any given finite size, but
have size 0 by default. For example, "/dev/zero/:100" means a fragment
of 100 NUL bytes.

.SH OPTIONS

This section enumerates the most interesting options to use with
\fBfuesfile\fR. See "man fuse" and "man mount" for more options.

.TP
\fB-dump\fR

The \fB-dump\fR "option" tells \fBfusefile\fR to print out the
applicable fragment sequence for the current setup, including the
overlay table, if any. The printout is done instead of setting up a
mount point.

.TP
\fB-o\fIallow_other\fB

The fuse option \fI-oallow_other\fR is needed for sharing the fused
file with other users who otherwise will not have access to it
(including "root"). Note however that this must first be enabled in
\fI/etc/fuse.conf\fR.

.TP
\fB-o\fInonempty\fR

The fuse option \fI-ononempty\fR may need to be used when reusing an
existing file as mountpoint.

.TP
\fB-o\fIuid=...\fR and \fB-o\fIgid=...\fR, 

These mount options, where \fI...\fR is a user or group id
respectively, are useful for root when using \fBfusedisk\fR and
thereby give user or group ownership for the mount to the nominated
user or group.

.SH EXAMPLES
This section illustrates uses of \fBfusefile\fR.

.SS Exanple 1.
Insert a file "y" into a file "x" at position 1200.
.RS
\fB$ fusefile -ononempty x x/:1200 y x/1200:\fR
.RE
This also shadows the original file "x" and presents the fused file
instead.

.SS Example 2.
Make fused file y be a swap of the beginning and end of file "x", at
position 2442.
.RS
\fB$ fusefile y x/2442: x/:2442\fR
.RE

.SS Example 3.
Replace partition 2 of an image file, \fIA\fR, with a different
file, \fIX\fR. For this example the partition table looks as follows.
.RS
\fB$ partx -oNR,START,SECTORS \fIA\fR
    NR   START  SECTORS
     1    2048  2097152
     2 2099200   409600
     3 2508800 14268383
.RE
.br
As each sector is 512 bytes the clipping points around partition 2 are
1074790400 and 1284505600 and the insertion size is 209715200 bytes.
The \fBfusefile\fR comman will therefore be as follows.
.RS
\fB$ fusefile -ononempty \fIA\fB \fIA\fB/:1074790400 \fIX\fB/:209715200 \fIA\fB/1284505600\fR
.RE
Note that the fused file shadows the original file \fIA\fR.

.SS Example 4.
Protect raw disk image file with an overlay:
.RS
\fB$ fusefile -ononempty disk.raw -overlay:today disk.raw\fR
.RE
By that set up, the overlay file, "today", will protect the disk image
file, "disk.raw" from changes, and also override the pathname
"disk.raw" to be the fused file.

.SS Example 5.
A fusefile mount with an \fIoverlay file\fR is writable regardless of
the fused fragments, but all updates are written to the overlay file
instead of to the fragments.

.RS
\fB$ fusefile -ononempty \fIA\fR \fB-overlay:\fIB\fB \fIA\fR
.RE

The overlay file, \fIB\fR in the example, contains all changes to the
shadowed original file, \fIA\fR. The overlay file contains only the
newly written regions and content is otherwise obtained from the
original file.

To that end, the overlay file also contains a "marker table" at the
end as if appended to the fused file. That part of the file is outside
of the fused file; and it's simply an element count followed by pairs
of byte addresses that tell which regions of the fused file have been
captured into the overlay file. (The marker table is of course
maintained so that adjoining regions are collapsed)

Thus, an overlay file may be reused to later re-establish the same
fused file with overlay as previously, to continue capturing more
changes.

.SS Example 6.
As final example, we set up a fused block device \fIy\fR as a swap of
the beginning and end of file "x", at position 2442:
.RS
\fB$ sudo fusedisk -ouid=1000 y x/2442: x/:2442\fR
.RE
Note the use of \fBsudo\fR for becoming \fIroot\fR, which is required
for block device handling, and also the \fB-ouid=1000\fR option so as
to make the block device \fIy\fR be owned by the user with id 1000.

.SH NOTES

\fBfusefile\fR opens the nominated source files before any bind
mounting. With the fuse option \fI-ononempty\fR it will bind over an
non-empty file, which may be useful. The source files remain open, but
the source fragments are not recomputed. If a source file changes the
fused file will present the new content. If a source is reduced in
size, access will be inconsistent.

If the mountpoint file doesn't exist, then \fBfusefile\fR creates it.

Unmounting is done with "\fBfusermount -u\fR \fImountpoint\fR" as
usual. A \fBfusedisk\fR mount is unmounted by \fIroot\fR using
\fBumount\fR.

.SH SEE ALSO
\fBfuse, fusermount, mount, dmsetup\fR

.SH AUTHOR

Ralph Rönnquist <ralph.ronnquist@gmail.com>.

